﻿using Xamarin.Forms;
using System;
using System.Collections.Generic;
using Xamarin.Forms.Xaml;

namespace calculator
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new calculatorPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

    }
}
